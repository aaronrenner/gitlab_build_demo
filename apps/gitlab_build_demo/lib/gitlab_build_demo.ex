defmodule GitlabBuildDemo do
  @moduledoc """
  Documentation for GitlabBuildDemo.
  """

  @doc """
  Hello world.

  ## Examples

      iex> GitlabBuildDemo.hello()
      :world

  """
  def hello do
    :world
  end
end
