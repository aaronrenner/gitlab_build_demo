# Since configuration is shared in umbrella projects, this file
# should only configure the :gitlab_build_demo_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :gitlab_build_demo_web,
  generators: [context_app: false]

# Configures the endpoint
config :gitlab_build_demo_web, GitlabBuildDemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ApyeIJQRakWalevVL171R7vEAXoFOFF8215KBjoRI0sNIEiDk6Vtl9ZEnF6XfuU2",
  render_errors: [view: GitlabBuildDemoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: GitlabBuildDemoWeb.PubSub, adapter: Phoenix.PubSub.PG2]

config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
