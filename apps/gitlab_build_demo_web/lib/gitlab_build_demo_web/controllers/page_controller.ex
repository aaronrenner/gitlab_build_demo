defmodule GitlabBuildDemoWeb.PageController do
  use GitlabBuildDemoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
